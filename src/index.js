import Vue from 'vue';
import App from './App.vue';
import Tabs from './components/Tabs';

Vue.config.productionTip = false;
Vue.use(Tabs);

new Vue({
  el: '#app',
  render: h => h(App)
})